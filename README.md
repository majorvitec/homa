# Homa [2021]

Created a Card-Swiping App. Sound plays if User taps on a picture.

## Dependencies
- [flutter_tindercard](https://pub.dev/packages/flutter_tindercard)
- [audioplayers](https://pub.dev/packages/audioplayers)

## App Overview

### Cards Swiper Screen

![Cards Swiper Screen](/images/readme/cards_screen.png "Cards Swiper Screen")

### Cards Overview Screen

![Cards Overview Screen](/images/readme/cards_overview_screen.png "Cards Overview Screen")
