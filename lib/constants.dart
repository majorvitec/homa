import 'package:flutter/material.dart';

/* Colors */
const Color kLightSalmonRed = Color.fromRGBO(255, 160, 122, 1);
const Color kLightSalmonRedOP05 = Color.fromRGBO(255, 160, 122, 0.5);

const Color kSalmonRed = Color.fromRGBO(250, 128, 114, 1);
const Color kSalmonRedOP05 = Color.fromRGBO(250, 128, 114, 0.5);

const Color kDarkSalmonRed = Color.fromRGBO(233, 150, 122, 1);
const Color kDarkSalmonRedOP05 = Color.fromRGBO(233, 150, 122, 0.5);

const Color kLightCoralRed = Color.fromRGBO(240, 128, 128, 1);
const Color kLightCoralRedOP05 = Color.fromRGBO(240, 128, 128, 0.5);

const Color kIndianRed = Color.fromRGBO(205, 92, 92, 1);
const Color kIndianRedOP05 = Color.fromRGBO(205, 92, 92, 0.5);

const Color kCrimsonRed = Color.fromRGBO(220, 20, 60, 1);
const Color kCrimsonRedOP05 = Color.fromRGBO(220, 20, 60, 0.5);

const Color kFirebrickRed = Color.fromRGBO(178, 34, 34, 1);
const Color kFirebrickRedOP05 = Color.fromRGBO(178, 34, 34, 0.5);

const Color kRed = Color.fromARGB(255, 0, 0, 1);

const Color kDarkRed = Color.fromRGBO(139, 0, 0, 1);
const Color kDarkRedOP09 = Color.fromRGBO(139, 0, 0, 0.9);
const Color kDarkRedOP08 = Color.fromRGBO(139, 0, 0, 0.8);
const Color kDarkRedOP07 = Color.fromRGBO(139, 0, 0, 0.7);
const Color kDarkRedOP06 = Color.fromRGBO(139, 0, 0, 0.6);
const Color kDarkRedOP05 = Color.fromRGBO(139, 0, 0, 0.5);

const Color kMaroonRed = Color.fromRGBO(128, 0, 0, 1);
const Color kMaroonRedOP05 = Color.fromRGBO(128, 0, 0, 0.5);

const Color kTomatoRed = Color.fromRGBO(255, 99, 71, 1);
const Color kTomatoRedOP05 = Color.fromRGBO(255, 99, 71, 0.5);
