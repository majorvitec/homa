import 'package:flutter/cupertino.dart';

class Person {
  final String name;
  final String imagePath;
  final String soundPath;

  Person({
    @required this.name,
    @required this.imagePath,
    this.soundPath: '',
  });
}
