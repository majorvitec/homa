import 'package:flutter/material.dart';
import 'package:homa/screens/bottom_navigation_bar_screen.dart';
import 'package:homa/utils/portrait_mode_mixin.dart';

void main() {
  runApp(Homa());
}

class Homa extends StatelessWidget with PortraitModeMixin {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        fontFamily: 'KaushanScript',
      ),
      home: BottomNavigationBarScreen(),
    );
  }
}
